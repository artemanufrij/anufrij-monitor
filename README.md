# Monitor

### Donate
<a href="https://www.paypal.me/ArtemAnufrij">PayPal</a> | <a href="https://liberapay.com/Artem/donate">LiberaPay</a> | <a href="https://www.patreon.com/ArtemAnufrij">Patreon</a>

![screenshot](screenshots/screenshot.png)

## Install from Github

### Requirements
``` bash
# Debian based
sudo apt install git nodejs

# Arch based
sudo pacman -S git nodejs
```

### Build
``` bash
# clone this repository
git clone https://gitlab.com/artemanufrij/anufrij-monitor.git

# switch into project folder
cd anufrij-monitor

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# (optional) create an electron app for linux
npm run package-linux
```