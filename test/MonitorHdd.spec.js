import {
  shallowMount
} from '@vue/test-utils'
import MonitorHdd from '../src/components/MonitorHdd.vue'


describe('MonitorHdd.vue', () => {
  it("Test serverUrl property", () => {
    const wrapper = shallowMount (MonitorHdd, {
      propsData: {
        serverUrl: "my-server-url.org"
      }
    });

    expect(wrapper.props().serverUrl).toBe("my-server-url.org");
  });
})
