<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$devices = shell_exec ("df -h | grep -Po '^/dev/(sd|md).*'");
$devices = explode("\n", $devices);
sort ($devices);

$return_value = "[";
$i = 0;
foreach ($devices as $item) {
    while (strpos ($item, "  ") > -1) {
        $item = str_replace ("  ", " ", $item);
    }
    $elements = explode (" ", $item);

    if (count($elements)<6) {
        continue;
    }

        if ($i > 0) {
                $return_value .= ",";
        }

    $prozent = $elements[4];
    preg_match ('/\w*$/', $elements[5], $treffer);
    $device = $treffer[0];
    $storage = $elements[1];
    $storage_used = $elements[2];
    $storage_free = $elements[3];
    $percent = $elements[4];

    if (!empty ($prozent)) {
        if (substr ($item, 0, 7) === "/dev/md") {
                $return_value .= "\n\t{\n\t\t\"type\": \"".raid."\",\n\t\t";
        } else {
                $return_value .= "\n\t{\n\t\t\"type\": \"".disk."\",\n\t\t";

        }
        $return_value .= "\"title\": \"".$device."\",\n\t\t";
        $return_value .= "\"inUse\": \"".$storage_used."\",\n\t\t";
        $return_value .= "\"storage\": \"".$storage."\",\n\t\t";
        $return_value .= "\"percent\": \"".$percent."\"\n\t}";
    }
        $i++;
}

$return_value .= "\n]";
echo $return_value;

flush();
?>