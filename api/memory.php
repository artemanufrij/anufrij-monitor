<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$result = shell_exec ("free -m | grep 'Mem'");
while (strpos ($result, "  ") > -1) {
    $result = str_replace ("  ", " ", $result);
}
$elements = explode (" ", $result);
$gesamt = $elements[1];
$frei = $elements[6];
$prozent_wert = 100 - round($frei / $gesamt * 100, 0);

echo "{\n\t\"value\": ".$prozent_wert."\n}";

flush ();
?>