<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$return_value = "[";

$hdd_fan = trim(shell_exec ("cat /tmp/fan_hdd"));

$return_value .= "\n\t{\n\n\t\t\"title\":\"HDD\",\n\t\t\"io\": \"17\",\n\t\t\"state\": \"".($hdd_fan == 0 ? "stopped" : "running")."\"\n\t},";

$cpu_fan = trim(shell_exec ("cat /tmp/fan_cpu"));

$return_value .= "\n\t{\n\n\t\t\"title\":\"CPU\",\n\t\t\"io\": \"22\",\n\t\t\"state\": \"".($cpu_fan == 0 ? "stopped" : "running")."\"\n\t}\n]";

echo $return_value;

flush();
?>
