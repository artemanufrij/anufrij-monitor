<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$services = $interfaces = preg_split("/;/", $_GET["services"]);
sort ($services);

$return_value = "[";
$i = 0;
foreach ($services as $item) {
    $result = trim(shell_exec ("service ".$item." status | grep -Po '(?<=Active:\s)[\w\s(]*\)'"));

    if ($i > 0) {
        $return_value .= ",";
    }

    $return_value .= "\n\t{\n\t\t\"title\": \"".$item."\",\n\t\t";

    if (strpos ($result, "running") >- 1) {
        $return_value .= "\"state\": \"running\"\n";
    } else {
        $return_value .= "\"state\": \"stopped\"\n";
    }

    $return_value .= "\t}";
    $i++;
}

$return_value .= "\n]";
echo $return_value;

flush();
?>