<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$result = explode("\n", shell_exec ("cat /proc/mdstat | grep -Po '(md\d.*)|^\s*\d\d\d\d.*'"));

$return_value = "[";

$i = 0;

foreach ($result as $line) {
        if (!empty($line)) {
                if (substr ($line, 0, 2) == "md") {
                        if ($i > 0) {
                                $return_value .= "]\n\t},";
                        }

                        $items = preg_split("/:/", $line);

                        $return_value .= "\n\t{\n\t\t\"name\": \"".trim($items[0])."\",";
                        $return_value .= "\n\t\t\"lines\": [\"".$items[1]."\"";
                } else {
                        $return_value .= ",\"".trim($line)."\"";
                }
                $i++;
        }
}

$return_value .= "]\n\t}";

$return_value .= "\n]";

echo $return_value;
flush ();
?>