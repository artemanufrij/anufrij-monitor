<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$cpuinfo = trim(shell_exec("cat /proc/stat | grep -Po 'cpu .*'"));

while (strpos ($cpuinfo, "  ") > -1) {
    $cpuinfo = str_replace ("  ", " ", $cpuinfo);
}
$elements = explode (" ", $cpuinfo);

echo "{\n";
echo "\t\"user\": ".$elements[1].",\n";
echo "\t\"nice\": ".$elements[2].",\n";
echo "\t\"system\": ".$elements[3].",\n";
echo "\t\"idle\": ".$elements[4].",\n";
echo "\t\"iowait\": ".$elements[5].",\n";
echo "\t\"irq\": ".$elements[6].",\n";
echo "\t\"softirq\": ".$elements[7].",\n";
echo "\t\"steal\": ".$elements[8].",\n";
echo "\t\"guest\": ".$elements[9].",\n";
echo "\t\"guest_nice\": ".$elements[10];
echo "\n}";

flush ();
?>