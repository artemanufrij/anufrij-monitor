<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$return_value = "{\n\t\"hostname\": \"".strtoupper(gethostname())."\",\n\t\"information\": [";

$result = explode("\n", shell_exec ("lscpu"));

$i = 0;

foreach ($result as $line) {
        if (!empty($result[$i])) {
                if ($i > 0 ) {
                        $return_value .= ",";
                }

                $items = preg_split("/:/", $line);
                $return_value .= "\n\t\t{\"line\": \"".$i."\",";
                $return_value .= "\"name\": \"".$items[0]."\",";
                $return_value .= "\"value\": \"".trim($items[1])."\"}";

                $i++;
        }
}
$return_value .= "\n\t]";
$return_value .= "\n}";

echo $return_value;

flush ();
?>
