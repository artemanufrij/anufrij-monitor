<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$interfaces = preg_split("/;/", $_GET["ifaces"]);
sort ($interfaces);

$return_value = "[";
$i = 0;

foreach ($interfaces as $item) {
    if ($item != "") {
        if ($i > 0) {
            $return_value .= ",";
        }
        $rx = trim(shell_exec("ifconfig ".$item." | grep -Po 'RX .* bytes \d*' | grep -Po '\d*$'"));
        $tx = trim(shell_exec("ifconfig ".$item." | grep -Po 'TX .* bytes \d*' | grep -Po '\d*$'"));

        $return_value .= "\n\t{";
        $return_value .= "\n\t\t\"name\": \"".$item."\",";
        $return_value .= "\n\t\t\"rx\": ".$rx.",";
        $return_value .= "\n\t\t\"tx\": ".$tx;
        $return_value .= "\n\t}";
        $i++;
    }
}

$return_value .= "\n]";
echo $return_value;

flush();
?>