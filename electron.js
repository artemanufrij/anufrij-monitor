const url = require('url')
const path = require('path')
const electron = require('electron')
const {
  app,
  BrowserWindow
} = electron

let win = null;

app.on('ready', () => {
  let bounds = electron.screen.getPrimaryDisplay().bounds;

  const WINDOW_WIDTH = 1480;
  const WINDOW_HEIGHT = (bounds.height > 1024 ? 1024 : bounds.height - 64);

  let x = bounds.x + ((bounds.width - WINDOW_WIDTH) / 2);
  let y = bounds.y + ((bounds.height - WINDOW_HEIGHT) / 2);

  win = new BrowserWindow({
    width: WINDOW_WIDTH,
    height: WINDOW_HEIGHT,
    x: x,
    y: y
  });

  win.setMenu(null);

  //win.loadURL('http://localhost:8080');
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'dist', 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  win.on('closed', () => {
    win = null;
  });
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
})

app.on('window-all-closed', () => {
  if (process.platform != 'darwin') {
    app.quit();
  }
});
